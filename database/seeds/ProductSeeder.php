<?php

use Illuminate\Database\Seeder;
use App\Models\Admin\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      


          $faker = Faker\Factory::create();

          for($i = 0; $i < 10; $i++) {
              App\Product::create([
                  'name' => $faker->name,
                  'img' => $faker->img,
                  'url' => $faker->url
              ]);
          }

    }
}
