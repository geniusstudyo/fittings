<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use View;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
          View::composer(
            ['index','admin.index','site.page','site.products.index','site.products.show','site.order.complete','site.products.category','site.pages.index' ],'App\Views\MenuViewAll');

          View::composer(
            ['index','admin.index','site.page','site.products.index','site.products.show','site.order.complete','site.cart','site.products.category','site.pages.index'],'App\Views\PriceViewAll');

          View::composer(
            ['index','admin.index'],'App\Views\SliderViewAll');

          View::composer(
            ['index','admin.index','site.category_menu','admin.product.index','admin.product.create','admin.product.edit','site.pages.index'],'App\Views\CategoryViewAll');

          View::composer(
            ['index','admin.index'],'App\Views\ProductViewAll');

          View::composer(
            ['admin.index','admin.menu.index','admin.page.index','admin.category.index','admin.slider.index','admin.price.index','admin.product.index','admin.order.sortNew','admin.order.sortAssecc'],'App\Views\OrderViewAll');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
