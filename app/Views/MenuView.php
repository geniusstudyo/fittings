<?php

namespace App\Views;

use App\Models\Admin\Menu;
use View;

class MenuViewAll {

    protected $menus;
    // sssd
    public function __construct(Menu $menus){

        $this->menus = $menus;
    }

    public function compose($view){
        $view->with('menus', Menu::all());
    }


}
