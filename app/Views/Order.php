<?php

namespace App\Views;

use App\Models\Admin\Order;
use View;

class OrderViewAll {

    protected $orders;

    public function __construct(Order $orders){

        $this->orders = $orders;
    }

    public function compose($view){
        $view->with('orders', Order::all());
    }


}
