<?php

namespace App\Views;

use App\Models\Admin\Slider;

class SliderViewAll {

    protected $sliders;

    public function __construct(Slider $sliders){

        $this->sliders = $sliders;
    }

    public function compose($view){

        $view->with('sliders', Slider::all());
    }


}
