<?php

namespace App\Views;

use App\Models\Admin\Product;
use View;

class ProductViewAll {

    protected $products;

    public function __construct(Product $products){

        $this->products = $products;
    }

    public function compose($view){
        $view->with('products', Product::all());
    }


}
