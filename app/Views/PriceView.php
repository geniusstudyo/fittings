<?php

namespace App\Views;

use App\Models\Admin\Price;

class PriceViewAll {

    protected $price;

    public function __construct(Price $price){
      $this->price = $price;
    }

    public function compose($view){
      $view->with('price', Price::first());
    }
}
