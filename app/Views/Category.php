<?php

namespace App\Views;

use App\Models\Admin\Category;
use View;

class CategoryViewAll {

    protected $categories;

    public function __construct(Category $categories){

        $this->categories = $categories;
    }

    public function compose($view){
        $view->with('categories', Category::all());
    }


}
