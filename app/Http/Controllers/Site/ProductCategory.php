<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Product;
use App\Models\Admin\Category;


class ProductCategory extends Controller
{
    public function index()
    {
        return view('site.products.category');
    }

    public function show(Request $request)
    {
        $productscat = Product::productcategory($request->id)->get();

        return view('site.products.category', compact('productscat'));
    }
}
