<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Order;
use Turbosms;
use App\Http\Requests\Site\Order as OrderValidation;
use App\Models\Admin\OrderProduct;
use Cart;
use Redirect;

class OrderController extends Controller
{
    public function index(){

      return view('site.order.index');

    }

    public function store(OrderValidation $request){

      $order = Order::create($request->validated());

      foreach (Cart::getContent() as $item ) {

    			OrderProduct::create([

    				'product_id' => $item->id,
    				'order_id' => $order->id,
    				'count' => $item->quantity,
    				'price' => $item->price

    			]);

    		}

        $send = Turbosms::send('+380972388564', 'Новый заказ:'. $order->id .'|ФИО:'. $order->fio.'|Телефон:'. $order->phone  );

      Cart::clear();

      return view('site.order.complete', compact('order'));

    }
}
