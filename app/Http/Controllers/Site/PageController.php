<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Page;

class PageController extends Controller {

  public function index(Request $request,$url){

    $page = Page::where('url',$request->url)->first();

    return view('site.pages.index', compact('page'));
    
  }

}
