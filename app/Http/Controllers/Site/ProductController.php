<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Product;
use Cart;

class ProductController extends Controller
{
    public function index(){

      $products = Product::all();

      return view('site.products.index',compact('products'));
    }

    public function addToCart(Request $request){

      if($request->isMethod('post')){

          $carts = Cart::add([
              'id' => $request->product_id,
              'name' => $request->product_name,
              'price' => $request->product_price,
              'quantity' => $request->product_col,

            ]);

      }

    }

    public function getShowProduct($product){

        return view('site.products.show', compact('product'));
    }


}
