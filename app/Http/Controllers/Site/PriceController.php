<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Price;

class PriceController extends Controller
{
    public function download(Request $request, $price){

      $price = Price::where('price', $request->price)->first();

      return view('index', compact('price'));
    }
}
