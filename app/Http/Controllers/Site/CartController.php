<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Product;
use Cart;

class CartController extends Controller
{
    public function index()
    {
        return view('site.cart');
    }

    public function remove(Request $request)
    {
        Cart::remove($request->itemId);
        return back();
    }



    public function update(Request $request)
    {
        Cart::update($request->itemId, ['quantity'=> [
          'relative' => false,
          'value' => $request->product_col
        ]]);
    }
}
