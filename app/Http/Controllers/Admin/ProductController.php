<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Product;
use App\Http\Requests\Admin\Product as ProductRequest;
use App\Servises\ImageUpload;
use Redirect;
use Lang;
use Session;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();

        return view('admin.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request, ImageUpload $images)
    {

      $downloadedImagePath = $images->download($request);
      $downloadedPrintsPath = $images->downloadPrints($request);

      $data = $request->validated();

      $data['img'] = $downloadedImagePath;
      $data['prints'] = $downloadedPrintsPath;

      Product::create($data);

      Session::flash('message', Lang::get('message.add'));

      return back();
      // return Redirect::route('product.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($product)
    {
        return view('admin.product.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($product)
    {

        return view('admin.product.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(ProductRequest $request, $product, ImageUpload $images)
    {

        if($request->hasFile('img') && $request->hasFile('prints')){

          $downloadedImagePath = $images->download($request);
          $downloadedPrintsPath = $images->downloadPrints($request);

          $data = $request->validated();

          $data['img'] = $downloadedImagePath;
          $data['prints'] = $downloadedPrintsPath;

      	}

        $product->update($request->validated());

        return Redirect::back()->with('message', Lang::get('message.update_success'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($product)
    {

        $product->delete();

        return Redirect::back()->with('message', Lang::get('message.delete'));
    }
}
