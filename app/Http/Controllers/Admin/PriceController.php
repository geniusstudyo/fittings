<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\Admin\Price as PriceRequest;
use App\Http\Controllers\Controller;
use App\Models\Admin\Price;
use App\Servises\ImageUpload;
use Redirect;
use Lang;
use Session;

class PriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $prices = Price::all();

        return view('admin.price.index', compact('prices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.price.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PriceRequest $request, ImageUpload $images)
    {


        $images->downlodaPrice($request);

        Price::update($request->where('id',21));

        Session::flash('message', Lang::get('message.add'));

        return Redirect::route('price.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($price)
    {
        return view('admin.price.show', compact('price'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($price)
    {
        return view('admin.price.edit', compact('price'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $price, ImageUpload $file)
    {

        if($request->hasFile('link')){
          $downloadedImagePath = $file->downlodaPrice($request);
          $data = $request->all();
          $data['link'] = $downloadedImagePath;
        }

        $price->update($data);

        return back()->with('message', Lang::get('message.update_success'));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($price)
    {
        $price->delete();

        return back()->with('message', Lang::get('message.delete'));
    }
}
