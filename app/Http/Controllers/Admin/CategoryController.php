<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Category;
use App\Http\Requests\Admin\Category as CategoryRequest;
use App\Servises\ImageUpload;
use Redirect;
use Lang;
use Session;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = Category::with('product')->get();

        return view('admin.category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request, ImageUpload $images)
    {

        $downloadedImagePath = $images->download($request);

        $data = $request->validated();

        $data['img'] = $downloadedImagePath;

        Category::create($data);

        Session::flash('message', Lang::get('message.add'));

        return Redirect::route('category.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($category)
    {
        return view('admin.category.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($category)
    {
        return view('admin.category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, $category, ImageUpload $images)
    {
        if($request->hasFile('img')){

          $downloadedImagePath = $images->download($request);

          $data = $request->validated();

          $data['img'] = $downloadedImagePath;

        }

        $category->update($data);

        return Redirect::back()->with('message', Lang::get('message.update_success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($category)
    {

        $category->delete();

        Session::flash('message', Lang::get('message.delete'));

        return Redirect::back();
    }
}
