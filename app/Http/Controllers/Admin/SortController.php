<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Order;
use App\Servises\Sort;
use Redirect;
use Lang;
use Session;


class SortController extends Controller {


      public function sortNew()
      {

          $sortNew = Order::new()->get();


          return view('admin.order.sortNew', compact('sortNew'));
      }

      public function sortAssecc()
      {

          $sortAssecc = Order::assecc()->get();

          return view('admin.order.sortAssecc', compact('sortAssecc'));
      }
}
