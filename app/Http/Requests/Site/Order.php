<?php

namespace App\Http\Requests\Site;

use Illuminate\Foundation\Http\FormRequest;

class Order extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fio' => 'required',
            'phone' => 'required|numeric',
            'city' => 'required',
            'street' => 'required',
            'payments' => 'required',


        ];
    }

    public function messages (){

      return [
        'name.required' => 'Поле (Название категории) обязательно к заполнению',
        'phone.required.numeric' => 'Поле (Фото категории) может содержать только цифры. ',
        'city.required' => 'Поле :attribute обязательно к заполнению',
        'street.required' => 'Поле :attribute обязательно к заполнению',
        'payments.required' => 'Поле :attribute обязательно к заполнению',
      


      ];
    }


}
