<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class Slider extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'img' => 'required|file',
            'url' => 'required',
        ];
    }

    public function messages (){

      return [
        'name.required' => 'Поле (Имя слайдера) обязательно к заполнению',
        'img.file' => 'Поле (Прайс) может содержать только латинские буквы. ',
        'url.required' => 'Поле (Имя слайдера) обязательно к заполнению',

      ];
    }


}
