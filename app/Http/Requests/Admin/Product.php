<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class Product extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'description' => 'required',
            'price' => 'required',
            'category_id' => 'required',
            'img' => 'file',
            'prints' => 'file',
            'url' => 'required',
            'alias' => 'required',
            'seo_title' => 'required',
            'seo_desc' => 'required',
            'seo_keywords' =>'required',
        ];
    }

    public function messages (){

      return [
        'name.required' => 'Поле (Название товара) обязательно к заполнению',
        'description.required' => 'Поле :attribute обязательно к заполнению',
        'category_id.required' => 'Укажите категорию',
        'price.required' => 'Поле :attribute обязательно к заполнению',
        'img.file' => 'Поле (Фото товара) обязательно к заполнению',
        'prints.file' => 'Поле (Фото чертеж товара) обязательно к заполнению',
        'url.required' => 'Поле :attribute обязательно к заполнению',
        'alias.required' => 'Поле :attribute обязательно к заполнению',
        'seo_title.required' => 'Поле :attribute обязательно к заполнению',
        'seo_desc.required' => 'Поле :attribute обязательно к заполнению',
        'seo_keywords.required' =>'Поле :attribute обязательно к заполнению',

      ];
    }


}
