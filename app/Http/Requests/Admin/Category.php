<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class Category extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'desc' => 'required',
            'url' => 'required',
            'img' => 'file',
            'alias' => 'required',
            'title' => 'required',
            'description' => 'required',
            'keywords' =>'required',
        ];
    }

    public function messages (){

      return [
        'name.required' => 'Поле (Название категории) обязательно к заполнению',
        'img.file' => 'Поле (Фото категории) может содержать только латинские буквы. ',
        'desc.required' => 'Поле :attribute обязательно к заполнению',
        'url.required' => 'Поле :attribute обязательно к заполнению',
        'alias.required' => 'Поле :attribute обязательно к заполнению',
        'title.required' => 'Поле :attribute обязательно к заполнению',
        'description.required' => 'Поле :attribute обязательно к заполнению',
        'keywords.required' =>'Поле :attribute обязательно к заполнению',

      ];
    }


}
