<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class Menu extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'url' => 'required|alpha',
            'alias' => 'required|alpha',
            'title'=> 'max:255',
            'description' => 'max:255',
            'keywords' => 'max:255'

        ];
    }

    public function messages (){

      return [
        'url.alpha' => 'Поле :attribute может содержать только латинские буквы. ',
        'alias.alpha' => 'Поле :attribute  может содержать только латинские буквы.  '
      ];
    }


}
