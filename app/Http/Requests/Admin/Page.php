<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class Page extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'url' => 'required',
            'text' => 'required',
            'seo_title' => 'required',
            'seo_desc' => 'required',
            'seo_keywords' =>'required',
        ];
    }

    public function messages (){

      return [
        'name.required' => 'Поле (Название категории) обязательно к заполнению',
        'url.required' => 'Поле :attribute обязательно к заполнению',
        'text.required' => 'Поле :attribute обязательно к заполнению',
        'seo_title.required' => 'Поле :attribute обязательно к заполнению',
        'seo_desc.required' => 'Поле :attribute обязательно к заполнению',
        'seo_keywords.required' =>'Поле :attribute обязательно к заполнению',

      ];
    }


}
