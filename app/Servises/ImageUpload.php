<?php
namespace App\Servises;

use Illuminate\Http\Request;
use App\Models\Admin\Slider;
use Intervention\Image\ImageManagerStatic as Image;

class ImageUpload {

  protected $image;

  public function __construct(Image $image){

      $this->image = $image;

  }

  public function download(Request $request){

    $photo =   $request->file("img")->getClientOriginalName();

    $photoProductResize = $this->image->make($request->file("img"))
                    ->resize(400, 400)
                    ->save(public_path($photo));

    $resultImg = str_replace('/public/','/',$photo);

    return $resultImg;

  }


  public function downloadPrints(Request $request){

      $photoPrints =   $request->file("prints")->getClientOriginalName();

      $photoProductResize = $this->image->make($request->file("prints"))
                      ->resize(400, 400)
                      ->save(public_path($photoPrints));

      $prints_img = str_replace('/public/','/',$photoPrints);

      return $prints_img;
    }

    public function downlodaPrice(Request $request){

      $file = $request->file('link')->getClientOriginalName();
      $path = 'public';
      $files = $request->file('link')->move($path,$file);

      $priceFile = str_replace('/public/','/',$file);

      return $priceFile;

    }

}
