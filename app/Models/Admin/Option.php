<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    public function product()
    {
        return $this->belongToMany('App\Models\Admin\Product');
    }
}
