<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = ['name','url','seo_title','seo_desc','seo_keywords','text'];


}
