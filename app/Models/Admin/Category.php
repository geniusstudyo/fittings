<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $fillable =['name','desc','url','img','alias','title','description','keywords'];

    public function product()
    {
        return $this->hasMany('App\Models\Admin\Product');
    }
}
