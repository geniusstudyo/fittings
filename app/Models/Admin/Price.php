<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $fillable = ['link'];
}
