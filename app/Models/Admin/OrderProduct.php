<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;


class OrderProduct extends Model
{
    protected $table = 'order_products';

    protected $fillable = ['order_id', 'product_id','count','price'];

    public function order(){

        return $this->belongsTo('App\Models\Admin\Order', 'order_id');

    }

    public function scopeNew($query){

        return $query->where('status',0);

    }

    public function scopeAssecc($query){

        return $query->where('status',1);

    }

}
