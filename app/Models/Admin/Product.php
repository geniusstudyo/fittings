<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Product extends Model
{
    protected $fillable = ['name','description','price','category_id','url','alias','seo_title','seo_desc','seo_keywords','img','prints'];

    public function category()
    {
        return $this->belongsTo('App\Models\Admin\Category');
    }

    public function scopeProductCategory($query,$category_id)
    {

      return $query->where('category_id', $category_id);
    }

}
