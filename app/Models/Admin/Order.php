<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

     protected $fillable = ['fio','phone','city','street','comment','status','payments'];

     protected $dates = ['created_at','updated_at'];

     public function product()
     {
         return $this->hasMany('App\Models\Admin\OrderProduct','order_id');
     }

     public function scopeNew($query)
     {
        return $query->where('status',0);
     }

     public function scopeAssecc($query)
     {
        return $query->where('status',1);
     }

}
