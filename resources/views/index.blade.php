@extends('app')
@include ('site.application')

@section ('slider')
  @include('site.slider')
@show

@section('category')
  @include('site.category')
@show

@include('site.content')

@section('content')

@show

@section('footer')
  @include('site.footer')
@show
