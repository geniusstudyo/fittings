{!! Form::model($category, ['method' => 'PATCH','files' => true,'id'=>'form_menu_request' , 'action' => ['Admin\CategoryController@update', $category->id]]) !!}

	@include ('admin.category.form', ['button' => 'Изменить'])

{!! Form::close() !!}
