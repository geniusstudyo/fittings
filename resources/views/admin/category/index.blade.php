@extends ('admin.app')

@include ('admin.top_menu')
<table class="uk-container uk-table uk-table-middle uk-table-striped">
    <thead>
        <tr>
            <th>Название</th>
            <th>URL Адрес</th>
            <th>Alias </th>
            <th>Описание </th>
            <th>Фото </th>
            <th>SEO Title</th>
            <th>SEO Description</th>
            <th>SEO Keywords</th>
            <th></th>
            <th></th>

        </tr>
    </thead>
    <tbody>
      @foreach ($categories as $category)
       <tr>
            <td> <a href="{{route('category.edit', $category->id)}}" class="uk-link-reset">{{$category->name}}</a></td>
            <td>{{$category->url}}</td>
            <td>{{$category->alias}}</td>
            <td>{{$category->desc}}</td>
            <td><img src="{{asset($category->img)}}" width="200"/></td>
            <td>{{$category->title}}</td>
            <td>{{$category->description}}</td>
            <td>{{$category->keywords}}</td>
            <td>
              <a href="{{route('category.edit', $category->id)}}" class="uk-link-reset"
              uk-icon="icon: file-edit" style="margin-top:8px;">
            </a>

            </td>
            <td>
              {!! Form::open(['method' => 'DELETE', 'route'=> ['category.destroy',$category->id]]) !!}
                  <button class="uk-button uk-button-link" uk-tooltip="Удалить" onclick="return confirm('Вы уверены что хотите удалить пункт категорию товаров?')">
                      <span class="uk-icon-button " uk-icon="icon: trash;  ratio: 1"></span>
                  </button>

              {!! Form::close() !!}
            </td>

        </tr>
      @endforeach
    </tbody>
</table>
