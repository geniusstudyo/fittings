@extends ('admin.app')
<div class="uk-margin-small">

<div class="uk-container-small uk-align-center uk-width-1-3" style="border:3px solid #ffa900;padding:20px;">

@if (Route::is('category.create'))

  <h4>Добавить категорию</h4>

@else (Route::is('category.edit'))

  <h4>Редактировать категорию</h4>

@endif

<lable >Название</lable>
{!! Form::text('name', null, ['class="uk-input uk-margin-small"']) !!}

<lable >Описание</lable>
{!! Form::text('desc', null, ['class="uk-input uk-margin-small"']) !!}

<lable >URL адрес</lable>
{!! Form::text('url', null, ['class="uk-input uk-margin-small"']) !!}

<lable >Алиас</lable>
{!! Form::text('alias', null, ['class="uk-input uk-margin-small"']) !!}

<lable>Фото категории</lable>
{!! Form::file('img', null, ['class="uk-input uk-margin-small" ']) !!}

<lable >SEO Title</lable>
{!! Form::text('title', null, ['class="uk-input uk-margin-small"']) !!}

<lable >SEO Description</lable>
{!! Form::text('description', null, ['class="uk-input uk-margin-small"']) !!}

<lable >SEO keywords</lable>
{!! Form::text('keywords', null, ['class="uk-input uk-margin-small"']) !!}





{!! Form::submit($button, ['class="uk-button uk-button-primary uk-button-large uk-margin-small"']) !!}

@include ('admin.validation')

{!! JsValidator::formRequest('App\Http\Requests\Admin\Category', '#form_menu_request') !!}

</div>

</div>
