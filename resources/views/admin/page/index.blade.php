@extends ('admin.app')
@include ('admin.top_menu')
<table class="uk-container uk-table uk-table-striped">
    <thead>
        <tr>
            <th>Название</th>
            <th>URL Адрес</th>
            <th>Alias </th>
            <th>SEO Title</th>
            <th>SEO Description</th>
            <th>SEO Keywords</th>
            <th></th>
            <th></th>

        </tr>
    </thead>
    <tbody>
      @foreach ($pages as $page)
       <tr>
            <td> <a href="{{route('page.edit', $page->id)}}" class="uk-link-reset">{{$page->name}}</a></td>
            <td>{{$page->url}}</td>
            <td>{{$page->alias}}</td>
            <td>{{$page->seo_title}}</td>
            <td>{{$page->seo_description}}</td>
            <td>{{$page->seo_keywords}}</td>
            <td>
              <a href="{{route('page.edit', $page->id)}}" class="uk-link-reset"
              uk-icon="icon: file-edit" style="margin-top:8px;">
            </a>


            </td>
            <td>
              {!! Form::open(['method' => 'DELETE', 'route'=> ['page.destroy',$page->id]]) !!}
                  <button class="uk-button uk-button-link" uk-tooltip="Удалить" onclick="return confirm('Вы уверены что хотите удалить пункт страницу?')">
                      <span class="uk-icon-button " uk-icon="icon: trash;  ratio: 1"></span>
                  </button>

              {!! Form::close() !!}
            </td>

        </tr>
      @endforeach
    </tbody>
</table>
