@extends ('admin.app')





<div class="uk-margin-small uk-background-default">

<div class="uk-container uk-align-center" style="border:3px solid #ffa900;padding:20px;">

@if (Route::is('page.create'))

  <h4>Добавить страницу</h4>

@else (Route::is('page.edit'))

  <h4>Редактировать страницу</h4>

@endif

<lable >Название</lable>
{!! Form::text('name', null, ['class="uk-input uk-margin-small"']) !!}

<lable >URL</lable>
{!! Form::text('url', null, ['class="uk-input uk-margin-small"']) !!}

<lable >SEO Title</lable>
{!! Form::text('seo_title', null, ['class="uk-input uk-margin-small"']) !!}

<lable >SEO Description</lable>
{!! Form::text('seo_desc', null, ['class="uk-input uk-margin-small"']) !!}

<lable >SEO keywords</lable>
{!! Form::text('seo_keywords', null, ['class="uk-input uk-margin-small"']) !!}

<lable>Страница</lable></lable>
{!! Form::select('menu', [$menu->pluck('name')], null, ['class="uk-input uk-margin-small"']) !!}

<lable >Текст</lable>
{!! Form::textarea('text', null, ['class="uk-input uk-margin-small" id="article-ckeditor"']) !!}

{!! Form::submit($button, ['class="uk-button uk-button-primary uk-button-large uk-margin-small"']) !!}

@include ('admin.validation')

{!! JsValidator::formRequest('App\Http\Requests\Admin\Page', '#form_menu_request') !!}

</div>

</div>
    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>
    <script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
    <script>
      var options = {
        filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
        filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
        filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
        filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
      };
    </script>
    <script>
    CKEDITOR.replace( 'article-ckeditor', options );
  </script>
