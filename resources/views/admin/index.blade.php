<!DOCTYPE html>
<html>
    <head>
        <title>UI Admin - Clean and responsive administration panel</title>

        <meta charset="UTF-8">
        <meta name="description" content="Clean and responsive administration panel">
        <meta name="keywords" content="Admin,Panel,HTML,CSS,XML,JavaScript">
        <meta name="author" content="Erik Campobadal">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" href="css/uikit.min.css" />

		    <link rel="stylesheet" href="css/style.css" />

         <script src="js/uikit.min.js" ></script>
		     <script src="js/uikit-icons.min.js" ></script>
    </head>
    <body>

      @include ('admin.top_menu')

        <div class=" content-background">
            <div class="uk-section-small uk-section-default header">
                <div class="uk-container uk-container-large">
                    <h1><span class="ion-speedometer"></span> Dashboard</h1>
                    <p>
                        Добро пожаловать в админ панель 2F
                    </p>

                </div>
            </div>
            <div class="uk-section-small">
                <div class="uk-container uk-container-large">
                    <div uk-grid class="uk-child-width-1-1@s uk-child-width-1-2@m uk-child-width-1-4@xl">
                        <div>
                            <div class="uk-card uk-card-default uk-card-body">
                                <span class="statistics-text">Новые Заказы</span><br />
                                <span class="statistics-number">
                                  <a href="{{route('order.index')}}">{{$orders->where('status',0)->count()}}</a>
                                    <span class="uk-label uk-label-success">
                                        8% <span class="ion-arrow-up-c"></span>
                                    </span>
                                </span>
                            </div>
                        </div>
                        <div>
                            <div class="uk-card uk-card-default uk-card-body">
                                <span class="statistics-text">Товары</span><br />
                                <span class="statistics-number">
                                    {{$products->count()}}
                                    <span class="uk-label uk-label-danger">
                                        13% <span class="ion-arrow-down-c"></span>
                                    </span>
                                </span>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
		<!-- Load More Javascript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.min.js" integrity="sha256-UGwvyUFH6Qqn0PSyQVw4q3vIX0wV1miKTracNJzAWPc=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.transit/0.9.12/jquery.transit.min.js" integrity="sha256-rqEXy4JTnKZom8mLVQpvni3QHbynfjPmPxQVsPZgmJY=" crossorigin="anonymous"></script>
		<script src="js/notyf.min.js"></script>
		<!-- Required Overall Script -->
        <script src="js/script.js"></script>
		<!-- Status Updater -->
		<script src="js/status.js"></script>
		<!-- Sample Charts -->
		<script src="js/charts.js"></script>
		<!-- Sample Notifications -->
		<script src="js/notification.js"></script>
    </body>
</html>
