@if (Session::has('message'))
<div class="uk-alert-success" uk-alert>
  <a class="uk-alert-close" uk-close></a>
    <p>
      {{Session::get('message')}}
    </p>
      </div>

@endif
