
<nav class="uk-navbar-container" style="background:#1e87f0;" uk-navbar>
  <div class="uk-navbar-left">
      <ul class="uk-navbar-nav ">
        <li>

          <a href="{{route('admin.index')}}" style="color:#fff;"  class="uk-icon-link" href="admin" >  <span uk-icon="icon: uikit;ratio: 2;" ></span></a>
        </li>
          <li>
             <a style="color:#fff;"  class="uk-icon-link" href="{{route('order.index')}}" >    <span uk-icon="icon: bell"   style="margin-right: 5px;"></span> Заказы
               <span class="uk-label uk-label-success" style="margin-left: 5px;">
               {{$orders->where('status',0)->count()}}
              </span>
            </a>
          </li>

          <li>
             <a style="color:#fff;"  class="uk-icon-link" href="{{route('product.index')}}" >  <span uk-icon="icon: grid" style="margin-right: 5px;"></span>Товары</a>
          </li>

          <li>
             <a style="color:#fff;"  class="uk-icon-link" href="{{route('menu.index')}}" >    <span uk-icon="icon: menu" style="margin-right: 5px;"></span>Меню сайта</a>
          </li>

          <li>
             <a style="color:#fff;"  class="uk-icon-link" href="{{route('price.index')}}" >    <span uk-icon="icon: file" style="margin-right: 5px;"></span>Прайс лист</a>
          </li>

          <li>
             <a style="color:#fff;"  class="uk-icon-link" href="{{route('category.index')}}"> <span uk-icon="icon: hashtag" style="margin-right: 5px;"></span>Категории</a>
          </li>

          <li>
             <a style="color:#fff;"  class="uk-icon-link" href="{{route('slider.index')}}"> <span uk-icon="icon: image" style="margin-right: 5px;"></span>Слайдеры</a>
          </li>

          <li>
             <a style="color:#fff;"  class="uk-icon-link" href="{{route('page.index')}}" > <span uk-icon="icon: info" style="margin-right: 5px;"></span>Инфо страницы</a>
          </li>
          <li>
            <div class="uk-button uk-button-default uk=padding-small" style="color:#fff;align:right;margin-top:18px;font-weight:bold;">
              @if (\Route::current()->getName() == 'order.index')
              <a  href="{{route('order.create')}}" class="uk-link-reset">Добавить заказ</a>

              @elseif (\Route::current()->getName() == 'product.index')
              <a  href="{{route('product.create')}}" class="uk-link-reset">Добавить товар</a>

              @elseif (\Route::current()->getName() == 'menu.index')
              <a  href="{{route('menu.create')}}" class="uk-link-reset">Добавить пункт меню</a>

              @elseif (\Route::current()->getName() == 'price.index')
              <a  href="{{route('price.create')}}" class="uk-link-reset">Обновить прайс</a>

              @elseif (\Route::current()->getName() == 'category.index')
              <a  href="{{route('category.create')}}" class="uk-link-reset">Добавить категорию</a>

              @elseif (\Route::current()->getName() == 'slider.index')
              <a  href="{{route('slider.create')}}" class="uk-link-reset">Добавить слайдер</a>

              @elseif (\Route::current()->getName() == 'page.index')
              <a  href="{{route('page.create')}}" class="uk-link-reset">Добавить инфо страницу</a>

              @else

                
              @endif
            </div>
          </li>

      </ul>

  </div>
</nav>
