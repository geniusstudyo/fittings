@extends ('admin.app')
@include ('admin.top_menu')
<table class="uk-container uk-table uk-table-striped">
    <thead>
        <tr>

            <th>Ссылка на файл</th>

            <th>Дата обновления</th>

            <th></th>

        </tr>
    </thead>
    <tbody>
      @foreach ($prices as $price)
       <tr>

            <td>{{$price->link}}</td>
            <td>{{$price->updated_at->format('d.m.Y')}}</td>

            <td>
              <a href="{{route('price.edit', $price->id)}}" class="uk-link-reset"
              uk-icon="icon: file-edit" style="margin-top:8px;">
            </a>


            </td>


        </tr>
      @endforeach
    </tbody>
</table>
