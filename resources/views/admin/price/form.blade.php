@extends ('admin.app')

<div class="uk-margin-small">

<div class="uk-container-small uk-align-center uk-width-1-3" style="border:3px solid #ffa900;padding:20px;">

@if (Route::is('price.create'))

  <h4>Добавить прайс</h4>

@else (Route::is('price.edit'))

  <h4>Редактировать прайс</h4>

@endif

<lable >Новый прайс</lable>
{!! Form::file('link', null, ['class="uk-input uk-margin-small"']) !!}


{!! Form::submit($button, ['class="uk-button uk-button-primary uk-button-large uk-margin-small"']) !!}

@include ('admin.validation')

{!! JsValidator::formRequest('App\Http\Requests\Admin\Price', '#form_menu_request') !!}

</div>

</div>
