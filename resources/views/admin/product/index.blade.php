@extends ('admin.app')

@include ('admin.top_menu')
<table class="uk-container-large uk-table uk-table-middle uk-table-striped">
    <thead>
        <tr>
            <th>Название</th>
            <th>Артикул</th>
            <th>Описание </th>
            <th>Цена </th>
            <th>Фото основное </th>
            <th>Фото чертежей </th>
            <th>Категория</th>

            <th></th>
            <th></th>

        </tr>
    </thead>
    <tbody>
      @foreach ($categories as $category)
          @foreach ($category->product as $product)
          @php
                  $path = str_replace('/admin','/'.$product->img, $product->img);

          @endphp
       <tr>
            <td> <a href="{{route('product.edit', $product->id)}}" class="uk-link-reset">{{$product->name}}</a></td>
            <td>111</td>
            <td>{{str_limit($product->description, 100)}}</td>
            <td>{{$product->price}}</td>
            <td><img src="{{asset($product->img)}}" width="200"/></td>
            <td><img src="{{asset($product->prints)}}" width="200"/></td>
            <td>{{$product->category->name}}</td>
            <td>
              <a href="{{route('product.edit', $product->id)}}" class="uk-link-reset"
              uk-icon="icon: file-edit" style="margin-top:8px;">
            </a>
            </td>
            <td>
              {!! Form::open(['method' => 'DELETE', 'route'=> ['product.destroy',$product->id]]) !!}
                  <button class="uk-button uk-button-link" uk-tooltip="Удалить" onclick="return confirm('Вы уверены что хотите удалить пункт категорию товаров?')">
                      <span class="uk-icon-button " uk-icon="icon: trash;  ratio: 1"></span>
                  </button>

              {!! Form::close() !!}
            </td>

        </tr>
      @endforeach
      @endforeach

    </tbody>
</table>
