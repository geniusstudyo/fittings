@extends ('admin.app')
<div class="uk-margin-small">

<div class="uk-container-large uk-align-center uk-width-1-2" style="border:3px solid #ffa900;padding:20px;">

@if (Route::is('product.create'))

  <h4>Добавить товар</h4>

@else (Route::is('product.edit'))

  <h4>Редактировать товар</h4>

@endif

<lable >Название</lable>
{!! Form::text('name', null, ['class="uk-input uk-margin-small"']) !!}

<lable >URL</lable>
{!! Form::text('url', null, ['class="uk-input uk-margin-small"']) !!}

<lable >Alias</lable>
{!! Form::text('alias', null, ['class="uk-input uk-margin-small"']) !!}

<lable >Описание</lable>
{!! Form::textarea('description', null, ['class="uk-input uk-margin-small" id="article-ckeditor"']) !!}

<lable >Категория</lable>

{!! Form::select('category_id', $categories->pluck('name', 'id'), 0, ['class="uk-select"'])!!}


<lable >Цена</lable>
{!! Form::text('price', null, ['class="uk-input uk-margin-small" ']) !!}
<br />
<lable>Фото товара</lable>
{!! Form::file('img', null, ['class="uk-input uk-margin-small" ']) !!}<br /><br />


<lable>Фото чертежей</lable>
{!! Form::file('prints', null, ['class="uk-input uk-margin-small" ']) !!}<br /><br />



<lable >SEO Title</lable>
{!! Form::text('seo_title', null, ['class="uk-input uk-margin-small"']) !!}

<lable >SEO Description</lable>
{!! Form::text('seo_desc', null, ['class="uk-input uk-margin-small"']) !!}

<lable >SEO keywords</lable>
{!! Form::text('seo_keywords', null, ['class="uk-input uk-margin-small"']) !!}

{!! Form::submit($button, ['class="uk-button uk-button-primary uk-button-large uk-margin-small"']) !!}

@include ('admin.validation')

{!! JsValidator::formRequest('App\Http\Requests\Admin\Product', '#form_menu_request') !!}

</div>

</div>


<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>

<script>
CKEDITOR.replace( 'article-ckeditor');
</script>
