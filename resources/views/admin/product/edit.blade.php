{!! Form::model($product, ['method' => 'PATCH','files' => true,'id'=>'form_menu_request' , 'action' => ['Admin\ProductController@update', $product->id]]) !!}

	@include ('admin.product.form', ['button' => 'Изменить'])

{!! Form::close() !!}
