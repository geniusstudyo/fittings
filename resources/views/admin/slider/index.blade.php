@extends ('admin.app')

@include ('admin.top_menu')
<table class="uk-container-large uk-table uk-table-middle uk-table-striped ">
    <thead>
        <tr>

            <th>Слайдер </th>

            <th></th>
            <th></th>

        </tr>
    </thead>
    <tbody>
      @foreach ($sliders as $slider)
       <tr>

            <td>  <a href="{{route('slider.edit', $slider->id)}}"><img src="{{asset($slider->img)}}" width="500"/>  </a></td>

            <td>




            </td>
            <td>
              {!! Form::open(['method' => 'DELETE', 'route'=> ['slider.destroy',$slider->id]]) !!}
                  <button class="uk-button uk-button-danger" uk-tooltip="Удалить" onclick="return confirm('Вы уверены что хотите удалить пункт категорию слайдер?')">
                    Удалить
                  </button>

              {!! Form::close() !!}
            </td>

        </tr>
      @endforeach
    </tbody>
</table>
