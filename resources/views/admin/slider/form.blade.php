@extends ('admin.app')
<div class="uk-margin-small">

<div class="uk-container-small uk-align-center uk-width-1-3" style="border:3px solid #ffa900;padding:20px;">

@if (Route::is('slider.create'))

  <h4>Добавить слайдер</h4>

@else (Route::is('slider.edit'))

  <h4>Редактировать слайдер</h4>

@endif

<lable >Название</lable>
{!! Form::text('name', null, ['class="uk-input uk-margin-small"']) !!}

<lable>Фото слайдера</lable>
{!! Form::file('img', null, ['class="uk-input uk-margin-small" ']) !!}

<lable>URL</lable>
{!! Form::text('url', null, ['class="uk-input uk-margin-small" ']) !!}


{!! Form::submit($button, ['class="uk-button uk-button-primary uk-button-large uk-margin-small"']) !!}

@include ('admin.validation')

{!! JsValidator::formRequest('App\Http\Requests\Admin\Slider', '#form_menu_request') !!}

</div>

</div>
