@extends ('admin.app')
@include ('admin.top_menu')
<table class="uk-container uk-table uk-table-striped">
    <thead>
        <tr>
            <th>Название</th>
            <th>URL Адрес</th>
            <th>Alias </th>
            <th>SEO Title</th>
            <th>SEO Description</th>
            <th>SEO Keywords</th>
            <th></th>
            <th></th>

        </tr>
    </thead>
    <tbody>
      @foreach ($menus as $menu)
       <tr>
            <td> <a href="{{route('menu.edit', $menu->id)}}" class="uk-link-reset">{{$menu->name}}</a></td>
            <td>{{$menu->url}}</td>
            <td>{{$menu->alias}}</td>
            <td>{{$menu->title}}</td>
            <td>{{$menu->description}}</td>
            <td>{{$menu->keywords}}</td>
            <td>
              <a href="{{route('menu.edit', $menu->id)}}" class="uk-link-reset"
              uk-icon="icon: file-edit" style="margin-top:8px;">
            </a>


            </td>
            <td>
              {!! Form::open(['method' => 'DELETE', 'route'=> ['menu.destroy',$menu->id]]) !!}
                  <button class="uk-button uk-button-link" uk-tooltip="Удалить" onclick="return confirm('Вы уверены что хотите удалить пункт меню?')">
                      <span class="uk-icon-button " uk-icon="icon: trash;  ratio: 1"></span>
                  </button>

              {!! Form::close() !!}
            </td>

        </tr>
      @endforeach
    </tbody>
</table>
