{!! Form::model($menu, ['method' => 'PATCH','id'=>'form_menu_request' , 'action' => ['Admin\MenuController@update', $menu->id]]) !!}

	@include ('admin.menu.form', ['button' => 'Изменить'])

{!! Form::close() !!}
