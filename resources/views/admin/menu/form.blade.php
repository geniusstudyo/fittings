@extends ('admin.app')
<div class="uk-margin-small">

<div class="uk-container-small uk-align-center uk-width-1-3" style="border:3px solid #ffa900;padding:20px;">

@if (Route::is('menu.create'))

  <h4>Добавить пункт меню</h4>

@else (Route::is('menu.edit'))

  <h4>Редактировать пункт меню</h4>

@endif

<lable >Название</lable>
{!! Form::text('name', null, ['class="uk-input uk-margin-small"']) !!}
<lable>Url адрес</lable>
{!! Form::text('url', null, ['class="uk-input uk-margin-small" ']) !!}
<lable>Алиас</lable>
{!! Form::text('alias', null, ['class="uk-input uk-margin-small" ']) !!}
<lable>SEO title</lable>
{!! Form::text('title', null, ['class="uk-input uk-margin-small"']) !!}

<lable>SEO description</lable>
{!! Form::textarea('description', null, ['class="uk-input uk-margin-small" ']) !!}

<lable>SEO keywords</lable>
{!! Form::text('keywords', null, ['class="uk-input uk-margin-small" ']) !!}
{!! Form::submit($button, ['class="uk-button uk-button-primary uk-button-large uk-margin-small"']) !!}

@include ('admin.validation')

{!! JsValidator::formRequest('App\Http\Requests\Admin\Menu', '#form_menu_request') !!}

</div>

</div>
