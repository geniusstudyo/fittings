@extends ('admin.app')
@include ('admin.top_menu')

@include('admin.order.sort')



<table class="uk-container uk-table uk-table-striped">
    <thead>
        <tr>
            <th>#Заказ</th>
            <th>ФИО</th>
            <th>Город \ Улица</th>
            <th>Телефон</th>
            <th>Комментарий</th>
            <th>Способ оплаты</th>
            <th>Статус заказа</th>
            <th>Удалить</th>
        </tr>
    </thead>
    <tbody>
      @foreach ($sortAssecc as $order)
       <tr>
            <td> <a href="{{route('order.edit', $order->id)}}" class="uk-link-reset">{{$order->id}}</a></td>
            <td>{{$order->fio}}</td>
            <td>{{$order->city}} {{$order->street}}</td>
            <td>{{$order->phone}}</td>
            <td>{{$order->comment}}</td>


            <td>

              @if ($order->payments == 'pay')
                  <div class="uk-label ">Наличные</div>
              @elseif ($order->payments == 'paymentBank')
                  <div class="uk-label uk-label-success">Приват Банк
              @else ($order->payments == 'paymentCheck')
                  <div class="uk-label uk-label uk-label-warning">Расчетный счет
              @endif

            </td>
            <td>
              @if ($order->status == 0)
                <div class="uk-button uk-button-danger uk-button-default">
                   Новый
                </div>
              @elseif ($order->status == 1)
              <div class="uk-button uk-button-primary">
                  Принят
              </div>
              @else

              @endif
              </td>
            <td>
              {!! Form::open(['method' => 'DELETE', 'route'=> ['order.destroy',$order->id]]) !!}
                  <button class="uk-button uk-button-link" uk-tooltip="Удалить" onclick="return confirm('Вы уверены что хотите удалить заказ?')">
                      <span class="uk-icon-button " uk-icon="icon: trash;  ratio: 1"></span>
                  </button>

              {!! Form::close() !!}
            </td>

        </tr>
      @endforeach
    </tbody>
</table>
