@extends ('admin.app')

<script
src="https://code.jquery.com/jquery-2.2.4.js"
integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
crossorigin="anonymous"></script>
<div class="uk-margin-small">

<div class="uk-container-small uk-align-center uk-width-1-3" style="border:3px solid #ffa900;padding:20px;">

@if (Route::is('order.create'))

  <h4>Добавить заказ</h4>

@else (Route::is('order.edit'))

  <h4>Редактировать заказ</h4>

@endif

{--!! Form::select('products', $products->pluck('name', 'id'), 0, ['onClick =>"alert(sddsds)"'])!!--}

<lable >Фамилия Имя Отчество</lable>
{!! Form::text('fio', null, ['class="uk-input uk-margin-small"']) !!}

<lable >Телефон</lable>
{!! Form::text('phone', null, ['class="uk-input uk-margin-small"']) !!}

<lable >Город доставки</lable></lable>
{!! Form::text('city', null, ['class="uk-input uk-margin-small"']) !!}

<lable >Улица доставки</lable>
{!! Form::text('street', null, ['class="uk-input uk-margin-small"']) !!}

<lable >Комментарий к заказу</lable>
{!! Form::text('comment', null, ['class="uk-input uk-margin-small"']) !!}


<lable >Способ оплаты</lable>
{!! Form::select('payments', ['pay' => 'Наличными','paymentBank' => 'Приват Банк', 'paymentCheck' => 'Расчетный счет'], 0, []) !!}

<lable >Статус заказа</lable>
{!! Form::select('status', ['0' => 'Новый','1' => 'Подтвержден'], $order->status, []) !!}


{!! Form::submit($button, ['class="uk-button uk-button-primary uk-button-large uk-margin-small"']) !!}

@include ('admin.validation')

{!! JsValidator::formRequest('App\Http\Requests\Admin\Order', '#form_menu_request') !!}

</div>

</div>
