@extends ('app')
<!-- UIkit CSS -->
<link rel="stylesheet" href="/css/uikit.css" />
<script
src="https://code.jquery.com/jquery-2.2.4.js"
integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
crossorigin="anonymous"></script>
@php

use App\Models\Admin\Product;

@endphp

<meta name="viewport" content="width=device-width, initial-scale=1.0">
@include ('site.category_menu')

<title>Корзина покупок</title>
<h1 class="uk-hidden@m uk-text-center">Ваш заказ</h1>
<table class="uk-table uk-overflow-auto uk-table-striped uk-table-middle" >
   <thead>
       <tr>
           <th class="uk-visible@l"></th>
           <th class="uk-visible@l"><b>Название</b></th>
           <th class="uk-visible@l"><b>Кол-во</b></th>
           <th class="uk-visible@l"><b>Цена</b></th>
           <th class="uk-visible@l"><b>Сумма</b></th>
           <th class="uk-visible@l"></th>
       </tr>
   </thead>
   <tbody>
      @foreach (Cart::getContent() as $product)
      @php

      $productImg = Product::where('id', $product->id)->first();

      @endphp

         <tr >
           <td class="uk-visible@l ">
              <img src="./{{$productImg->img}}" width="200" />
           </td>

           <td class="uk-visible@l ">
             {{$product->name}}
           </td>

        <td>
        <div class="uk-grid" uk-grid>
        <div class="uk-width-1-2 " style="padding-top:15px;padding-left:51px;">
        <form method="post" >
           <input type="hidden" name="itemId" value="{{$product->id}}">
           <input name="_token" type="hidden" value='{{ csrf_token() }}'>
           <div class="input-group input-number-group">
           <div class="input-group-button">
           <span class="input-number-decrement" onClick="UpdateQty($(this).closest('form'))">-</span>
           </div>
           <input class="input-number" type="number" name="product_col" value="{{$product->quantity}}" min="0" max="1000">
           <div class="input-group-button">
           <span class="input-number-increment" onClick="UpdateQty($(this).closest('form'))">+</span>
           </div>
           </div>
        </form>
        </div>
        </div>
        </td>

        <td>
          {{$product->price}} <i>$</i>
        </td>

        <td>
        <div class="uk-width-expand" style="margin-left: 50px;">
        <div>
             <p>
              {!! Form::open(['action' => ['Site\CartController@remove']]) !!}
              {!! Form::hidden('itemId', $product->id, []) !!}
              {!! Form::submit('Удалить', []) !!}
              {!! Form::close() !!}
            </p>
        </div>
        </div>
        </td>

        </div>
       </div>
   </div>
</div>


       </tr>
       @endforeach
   </tfoot>
</table>

<div class="uk-hr"></div>
<div class="uk-text-bold uk-float-right uk-padding-small">
        <div id="summ">К оплате: {{Cart::getTotal()}} $</div>
        <p></p>
        <a class="uk-button uk-button-primary uk-button-large" href="{{route('firstStepOrder')}}">Оформить заказ</a>
        </div>
</div>
</div>

<script>

function UpdateQty(form){

 }

 $(function(){

   $('.input-number-increment').click(function() {
   var $input = $(this).parents('.input-number-group').find('.input-number');
   var val = parseInt($input.val(), 10);
   $input.val(val + 1);
   var form = $(this).closest('form');

   $.post("/cart/update", $(form).serialize(), function(data){

     $.get("/cart", function(dd){

      $("#cols").html($(dd).find("#cols").html());
      $("#summ").html($(dd).find("#summ").html());
    })


   })
   return false;

});

$('.input-number-decrement').click(function() {
 var $input = $(this).parents('.input-number-group').find('.input-number');
 var val = parseInt($input.val(), 10);
 $input.val(val - 1);

 var form = $(this).closest('form');

   $.post("/cart/update", $(form).serialize(), function(data){

     $.get("/cart", function(dd){

      $("#cols").html($(dd).find("#cols").html());
      $("#summ").html($(dd).find("#summ").html());
    })


   })
   return false;
})


 });

</script>
