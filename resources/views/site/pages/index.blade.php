@extends ('app')
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="robots" content="index" />
  <meta name="description" content="{{$page->seo_desc}}">
  <meta name="keywords" content="{{$page->seo_keywords}}" />
</head>
<link rel="stylesheet" href="{{ asset ('css/uikit.css')}}" />

<title>{{$page->title}}</title>

@include('site.category_menu')
<div class="uk-container-small uk-align-center uk-margin-large uk-margin-top">

  <h2>{{$page->name}}</h2>
  <p>
    {!!$page->text!!}
  </p>

</div>

@include('site.footer')
