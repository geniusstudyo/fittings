<div style="background:#f8f8f8;">


<center><h2 class="uk-text-bold uk-padding-small">Продукция</h2></center>


<div class="uk-child-width-1-3@m uk-grid-small uk-grid-match" uk-grid>
    <!-- <h2>Продукция</h2> -->
    @foreach ($categories->take(3) as $category)
          <div >
            <div class="uk-card uk-card-body" >
                <img src="{{$category->img}}" style="border-radius:5px;" alt="{{$category->name}}" width="400"/>
                <h4 style="margin-top: 22px;margin-left: 28px;">
                <a href="{{$category->url}}"  class="uk-link-reset"><b>{{$category->name}}</b></a></h4>
                <p style="font-size:15px;margin-top: 22px;margin-left: 28px;">{{$category->desc}}</p>
            </div>
          </div>
    @endforeach
</div>
</div>
