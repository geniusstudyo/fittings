@extends ('app')

@include('site.top')

@include('site.menu')



<div class="uk-container">
  <div class="uk-grid uk-posit-top-center" uk-grid>
    <div class="uk-background-muted uk-box-shadow-small">


<div class="uk-article-title ">
  {{$page->name}}
</div>
<div class="uk-article">
  {!!$page->text!!}
</div>
    </div>
</div>

</div>
@include('site.footer')
