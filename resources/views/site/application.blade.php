<div class="site">
  <header class="site-header">
    <div class="layout-container layout-flex">
      <a href="/" class="site-title">2F<strong>фурнитура</strong></a>
      <div class="layout-flex-spacer"></div>
      <nav class="site-nav uk-visible@m">
        @foreach($menus as $menu)
        <a href="{{$menu->url}}" >{{$menu->name}}</a>
        @endforeach

      </nav>

<div class="uk-position-top uk-position-absolute">
      <div class="uk-offcanvas-content uk-hidden@l uk-hidden@m ">
      <div class="uk-grid uk-padding-small" style="background:#0d3475;color:#fff;" uk-grid>
          <div class="uk-width-1-2" style="padding-top:10px 0 10px 0;float:left;background:#fff;margin-top:-15px;margin-bottom:-14px;">
            <img src="logo.png" width="30" />
              <span style="color:#333">FITTING</span>
          </div>
          <!-- <div class="uk-width-1-3 ">
             <a  style="font-size:13px;" href="tel:+380685335454">+38(068)5335454</a>
          </div> -->
                  <span type="button" uk-toggle="target: #offcanvas-usage" class="uk-position-right" style="margin-top: 10px;margin-right: 19px;">
                  <span uk-icon="icon: menu; ratio: 2" style="float:right;"></span>
                  <div id="offcanvas-usage" uk-offcanvas>
                    <div class="uk-offcanvas-bar">
                        <ul class="uk-list uk-list-divider ">
                          @foreach ($categories as $category)
                          <li><a href="{{$category->id}}">{{$category->name}}</a></li>
                          @endforeach
                        </ul>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </div>
</div>

  </header>
  <section class="site-hero">
    <div class="site-hero__inner">
      <div class="layout-container">
        <div class="layout-row">
          <div class="layout-col">
            <div class="site-hero__content ">
              <h2 style="color:#fff;">Качественная <br />Фурнитура<br />Для Стекла <br />По низким <br />ценам</h2>
              <a href="/product" class="button button--ghost">Смотреть каталог</a>
              <a href="{{route('site.price.download',$price->link )}}" class="button button--ghost  ">Скачать прайс<span style="padding-left:5px;" uk-icon="icon: download"></span></a>
            </div>
          </div>

        </div>
      </div>
    </div>
  </section>
</div>
