@if (count($errors) > 0)
  @foreach ($errors->all() as $error)
    <div class="uk-alert uk-alert-danger">{{$error}}</div>
  @endforeach
@endif
