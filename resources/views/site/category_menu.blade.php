<div class="uk-visible@m ">

  <div class="uk-grid " uk-grid>
      <div class="uk-navbar-left uk-padding-left-large" >
        <a href="/" class="uk-link-reset"><span uk-icon="icon: uikit; ratio: 3" ></span> Фурнитура для стекла</a>

        <a href="{{route('site.price.download',$price->link )}}" class="uk-button uk-button-default uk-position-top-center" style="top: 19px;">Скачать прайс <span style="padding-left:5px;" uk-icon="icon: download"></span></a>
        <span class="uk-position-top-center" style="top:63px;font-size:12px;">Прайс обновлен: {{$price->updated_at->format('d.m.Y')}}</span>
      </div>

      <div class="uk-flex-middle uk-navbar-right uk-padding-small"  uk-grid>
        <span  class="uk-icon-link" uk-icon="icon: receiver"></span>
        <ul>
        <li style="font-size:14px;">+38 (097) 23 88 564</li>
        <li style="font-size:14px;">+38 (097) 23 88 564</li>
        <li style="font-size:12px;">(Пн.- Птн. с 10<sup>00</sup> - 20<sup>00</sup>)</li>
        </ul>
      </div>
  </div>

<div  style="background:#0d3475;">
    <nav class="uk-navbar-nav" uk-navbar>
            <ul class="uk-navbar-nav ">
                @foreach ($categories as $cat)

                  <li><a href="{{route('site.category.show',$cat->id)}}" style="color:#fff;">{{$cat->name}}</a></li>
                @endforeach
            </ul>
          <div id="cartId" class="uk-navbar-right" style="padding-right:40px;">
            <a style="color:#fff;" href="{{route('cart')}}" >
            <span uk-icon="icon: cart; ratio: 2" style="
         margin-left: 43px;"></span> <span class="uk-badge" id="cols">{{Cart::getTotalQuantity()}}  шт. {{--Cart::getSubTotal()--}} </span>
            </a>
        </div>
    </nav>
</div>
</div>

<div class="uk-hidden@m " >
  <div style="z-index: 980;" uk-sticky="bottom: #offset">
  <div style="background:#222;" class="uk-padding-small uk-grid">

    <div class="uk-width-expand">
      <button style="padding:0px;" class="uk-button uk-button-secondary" type="button" uk-toggle="target: #offcanvas-nav" >
        <span uk-icon="icon: menu;ratio:2;" ></span>
        <span style="font-size:14px;font-weight:normal;"></span>
      </button>
    </div>
    <div class="uk-width-1-2">
      <span style="float:right;color:#fff;margin-top: 4px;" >
      <span uk-icon="icon: receiver"></span>
      <a style="color:#fff;font-size:15px;padding-right: 14px;" href="tel:+380972388564" tel="+380972388564">+38 (097) 238 85 64</a>
      </span>
    </div>
    <div class="uk-width-auto">
      <div id="cartId" >
        <a style="color:#fff;" href="{{route('cart')}}" >
        <span class="uk-position-relative" uk-icon="icon: cart; ratio: 2" ></span>
        <span class="uk-badge" id="cols">
          {{Cart::getTotalQuantity()}}  {{--Cart::getSubTotal()--}}
        </span>
        </a>
    </div>
    </div>

  <div id="offcanvas-nav" uk-offcanvas="overlay: true">
    <div class="uk-offcanvas-bar">
      <ul style="font-size:14px;">

      <li style="margin:0px;">Звоните: </li>
          <li>+38 (097) 238 85 64</li>
          <li>+38 (097) 238 85 64</li>
        </ul>
        <ul class="uk-nav uk-nav-default">
            @foreach ($categories as $cat)
            <li style="border-bottom:1px solid #968a8a17;"><a href="#"><span class="uk-margin-small-right" uk-icon="icon: minus"></span> {{$cat->name}}</a></li>
            @endforeach

        </ul>
    </div>
</div>
</div>
</div>
</div>
