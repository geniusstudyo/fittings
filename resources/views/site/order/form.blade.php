<h2>Оформление заказа</h2>
{!! Form::open(['route' => 'storeStepOrder', 'id'=>'form_menu_request']) !!}
<div class="uk-margin">
  <div class="uk-form-controls">
    {!! Form::text('fio', null, ['class="uk-input" id="form-horizontal-text" required placeholder="ФИО"']) !!}
  </div>
</div>
<div class="uk-margin">
  <div class="uk-form-controls">
    {!! Form::text('phone', null, ['class="uk-input" id="form-horizontal-text" placeholder="Телефон" required']) !!}
  </div>
</div>
<div class="uk-margin">
  <div class="uk-form-controls">
    {!! Form::text('city', null, ['class="uk-input" id="form-horizontal-text" placeholder="Город доставки" required']) !!}
  </div>
</div>
{!! Form::text('street', null, ['class="uk-input" placeholder="Адрес доставки" required']) !!}
<div class="uk-margin-small" uk-grid>
  <div class="uk-width-1-2">
    {!! Form::radio('payments', 'pay', null, ['class="uk-radio"']) !!}
    <label for="nal"><b>Наличные</b></label><br />
    <p style="float:left;padding-left:25px;font-size:13px;margin-top:5px;">
      Оплата производится в офисе нашей компании г. Киев ул. Шевченка 3б
    </p>
  </div>
  <div class="uk-width-1-2" >
    <button class="uk-button uk-button-default" type="button" uk-toggle="target: .toggle-animation-queued; animation: uk-animation-fade; queued: true; duration: 300">Безналичный</button>
    <p class="toggle-animation-queued " hidden>
      {!! Form::radio('payments', 'paymentBank', null, ['class="uk-radio"']) !!}
      <span style="font-size:14px;">Приват банк</span>
      {!! Form::radio('payments', 'paymentCheck', null, ['class="uk-radio"']) !!}
        <span style="font-size:14px;">Расчетный счет</span>
    </p>

    <p style="float:left;padding-left:0px;font-size:13px;">
      Оплата производится на карту Приват Банка \ Расчетный счет
    </p>
    {!! Form::hidden('status', 0, []) !!}
  </div>
</div>
<div class="uk-margin">
  <label class="uk-form-label" for="form-horizontal-text">Комментарий к заказу</label>
  <div class="uk-form-controls">
    {!! Form::textarea('comment', null, ['class="uk-textarea" rows="5" placeholder=""']) !!}
  </div>
</div>
{!! Form::hidden('status', 1, []) !!}
<div class="uk-margin">
  <div class="uk-form-controls">
    {!! Form::submit('Заказ подтверждаю', ['class="uk-button uk-button-danger uk-button-large"']) !!}
  </div>
</div>

<!-- Scripts -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>

<!-- Laravel Javascript Validation -->
<script type="text/javascript" src="{{ url('vendor/jsvalidation/js/jsvalidation.js')}}"></script>


{!! JsValidator::formRequest('App\Http\Requests\Site\Order', '#form_menu_request') !!}
{!! Form::close() !!}
