@extends ('app')
<title>Оформление заказа</title>

<div style="border:2px solid #ccc;" class="uk-card uk-position-top-center uk-width-1-2 uk-visible@m"
>
    <div class="uk-container">

      @section ('error')
        @include ('site.errors.error')
      @show

      @section('form')
        @include('site.order.form')
      @show

    </div>

</div>

<!-- Mobil device visible -->
<div class="uk-card uk-position-top-center uk-hidden@l"
>
    <div class="uk-container">

      @section ('error')
        @include ('site.errors.error')
      @show

      @section('form')
        @include('site.order.form')
      @show

    </div>

</div>
