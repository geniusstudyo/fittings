@extends ('app')


@include('site.menu')

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<div class="uk-grid-collapse uk-position-top-center" uk-grid>
	<div class="uk-padding-large">
        <h1>Ваш заказ принят!</h1>
        <p class="uk-button-primary uk-padding-small uk-text-medium">Номер вашего заказа  № <b>{{$order->id}}</b></p>
        <p>Ожидайте звонка нашего менеджера. Наш менеджер свяжется с вами в течении рабочего дня.</p>
        <a href="/">Вернуться на главную</a>
    </div>
</div>

<div class="uk-position-bottom">
@include ('site.footer')
</div>
