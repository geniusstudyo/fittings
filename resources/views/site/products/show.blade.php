@extends  ('app')
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="robots" content="index" />
  <meta name="description" content="{{$product->seo_desc}}">
  <meta name="keywords" content="{{$product->seo_keywords}}" />
</head>

<script
src="https://code.jquery.com/jquery-2.2.4.js"
integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
crossorigin="anonymous"></script>

@include ('site.category_menu')
<title>{{$product->seo_title}}</title>
<div class="uk-child-width-1-2@s uk-grid-match" uk-grid>
    <div>
        <div class="uk-card uk-card-body">
          <a href="#modal-media-image" uk-toggle>
          <img src="../{{$product->img}}" width="400" height="400"/>
          </a>
          <div id="modal-media-image" class="uk-flex-top" uk-modal>
            <div class="uk-modal-dialog uk-width-auto uk-margin-auto-vertical">
              <button class="uk-modal-close-outside" type="button" uk-close></button>
              <img src="../{{$product->img}}" alt="">
            </div>
          </div>
        </div>
    </div>
    <div>
        <div class="uk-card uk-card-body">
          <div class=""  style="background:#eee;border-radius:10px;">
            <div>
                <div class="uk-panel " style="font-size: 14px;line-height: 1.5;padding: 0px;margin-bottom: 0px;background: #eafff7;padding: 31px;border-radius: 13px;">
                    <h3>{!!$product->name!!}</h3>

                      {!!$product->description!!}
                  <p style="float:left;padding:10px;">
                    <i>Цена: </i> <b style="font-size:25px;">{{$product->price}}</b>$
                  </p>
                  <form method="post" id="ajax_form"  class="uk-child-width-expand@s", uk-grid>
                    <input name="_token" type="hidden" value='{{ csrf_token() }}'>
                    <input name="product_id" type="hidden" value="{{$product->id}}">
                    <input name="product_name" type="hidden" value="{{$product->name}}">
                    <input name="product_price" type="hidden" value="{{$product->price}}">
                    <div class="input-group input-number-group">
                    <div class="input-group-button">
                      <span class="input-number-decrement">-</span>
                    </div>
                    <input class="input-number" type="number" name="product_col"  value="1" min="0" max="1000">
                    <div class="input-group-button">
                      <span class="input-number-increment">+</span>
                    </div>
                    </div>
                    <button  class="uk-button uk-button-primary uk-margin-left"
                    type="button" value="Заказать"
                    onClick="addToCart($(this).closest('form'));UIkit.notification({message: '<span uk-icon=\'icon: check\'></span> {!!$product->name!!}', pos: 'top-right',status: 'success',timeout: 1500}).close(immediate)" >Заказать</button>
                  </form>
                </div>
            </div>
            <h3 style="line-height: 0;padding: 0px;margin-bottom: 0px;background: #eee;padding: 31px;border-radius: 13px;">Чертежи:</h3>
                  <a href="#modal-media-image" uk-toggle>
                    <img src="../{{$product->prints}}" width="200"/>
                    <img src="../{{$product->prints}}"  width="200"/>
                  </a>
                  <div id="modal-media-image" class="uk-flex-top" uk-modal>
                    <div class="uk-modal-dialog uk-width-auto uk-margin-auto-vertical">
                      <button class="uk-modal-close-outside" type="button" uk-close></button>
                      <img src="../{{$product->prints}}" alt="">
                    </div>
                  </div>
                </div>

              </div>
        </div>
    </div>
</div>




<script>
  function addToCart(form){
    $.post("/product", $(form).serialize(), function(data){

      $.get("/cart", function(dd){
       $("#cartId").html($(dd).find("#cartId").html());
       // $("#cartIdMobil").html($(dd).find("#cartIdMobil").html());
     });


   });
    return false;
  }

  $(function(){

    $('.input-number-increment').click(function() {
  var $input = $(this).parents('.input-number-group').find('.input-number');
  var val = parseInt($input.val(), 10);
  $input.val(val + 1);
});

$('.input-number-decrement').click(function() {
  var $input = $(this).parents('.input-number-group').find('.input-number');
  var val = parseInt($input.val(), 10);
  $input.val(val - 1);
})


  })

</script>


@include ('site.footer')
