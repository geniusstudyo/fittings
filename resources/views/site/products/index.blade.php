@extends ('app')
<!-- UIkit CSS -->
<link rel="stylesheet" href="{{ asset ('css/uikit.css')}}" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
<script
src="https://code.jquery.com/jquery-2.2.4.js"
integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
crossorigin="anonymous"></script>
<!-- UIkit JS -->
<script src="/js/uikit.js"></script>
<script src="/js/uikit-icons.js"></script>


@include('site.category_menu')
<!-- Если существует первое вводи иначе второе представление -->
<!-- @includeFirst(['site.category_menu','site.test']) -->

<div class="uk-container uk-margin-top" >

  @foreach ($products as $product)
  <a href="{{route('site.product.show', $product->id)}}" class="uk-link-reset">
  <div class="uk-child-width-1-2@s" uk-grid>
      <div >
          <div >
            <img src="public/{{$product->img}}" width="400" style="float: right;"></div>
      </div>
        <div>
          <div class="uk-panel " style="font-size: 13px;line-height: 1;padding: 0px;margin-bottom: 0px;background: #eee;padding: 31px;border-radius: 13px;">
              <h3>{!!$product->name!!}</h3>

                {!!$product->description!!}
            <p style="float:left;padding:10px;">
              <i>Цена: </i> <b style="font-size:16px;"><u>{{$product->price}},00 $</u></b>
            </p>
            <form method="post" id="ajax_form"  class="uk-child-width-expand@s", uk-grid>
              <input name="_token" type="hidden" value='{{ csrf_token() }}'>
              <input name="product_id" type="hidden" value="{{$product->id}}">
              <input name="product_name" type="hidden" value="{{$product->name}}">
              <input name="product_price" type="hidden" value="{{$product->price}}">
              <div class="input-group input-number-group">
              <div class="input-group-button">
                <span class="input-number-decrement">-</span>
              </div>
              <input class="input-number" type="number" name="product_col"  value="1" min="0" max="1000">
              <div class="input-group-button">
                <span class="input-number-increment">+</span>
              </div>
              </div>
              <button  class="uk-button uk-button-primary uk-margin-left"
              type="button" value="Заказать"
              onClick="addToCart($(this).closest('form'));UIkit.notification({message: '<span uk-icon=\'icon: check\'></span> {!!$product->name!!}', pos: 'top-right',status: 'success',timeout: 1500}).close(immediate)" >Заказать</button>
            </form>

          </div>
      </div>

  </div>
  </a>
  <hr />



@endforeach
</div>


<script>
  function addToCart(form){
    $.post("/product", $(form).serialize(), function(data){

      $.get("/cart", function(dd){
       $("#cartId").html($(dd).find("#cartId").html());
       // $("#cartIdMobil").html($(dd).find("#cartIdMobil").html());
     });


   });
    return false;
  }

  $(function(){

    $('.input-number-increment').click(function() {
  var $input = $(this).parents('.input-number-group').find('.input-number');
  var val = parseInt($input.val(), 10);
  $input.val(val + 1);
});

$('.input-number-decrement').click(function() {
  var $input = $(this).parents('.input-number-group').find('.input-number');
  var val = parseInt($input.val(), 10);
  $input.val(val - 1);
})


  })

</script>
@include('site.footer')
