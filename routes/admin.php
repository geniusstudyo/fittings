<?php


Auth::routes();
Route::group(['middleware' => 'auth'], function (){
Route::get('/','Admin\IndexController@index')->name('admin.index');
Route::resource('/menu', 'Admin\MenuController');
Route::resource('/price', 'Admin\PriceController');
Route::resource('/slider', 'Admin\SliderController');
Route::resource('/category', 'Admin\CategoryController');
Route::resource('/product', 'Admin\ProductController');
Route::resource('/page', 'Admin\PageController');
Route::resource('/order','Admin\OrderController');
Route::get('/sortNew','Admin\SortController@sortNew')->name('sortNew');
Route::get('/sortAssecc','Admin\SortController@sortAssecc')->name('sortAssecc');
});
