<?php

Route::get('/','Site\IndexController@index');

Route::get('category', 'Site\ProductCategory@index')->name('site.category.index');
Route::get('category/{id}', 'Site\ProductCategory@show')->name('site.category.show');

Route::get('product', 'Site\ProductController@index')->name('site.product.index');
Route::post('product', 'Site\ProductController@addToCart')->name('site.product.cart');
Route::get('product/{product}','Site\ProductController@getShowProduct')->name('site.product.show');

Route::get('/cart/order','Site\OrderController@index')->name('firstStepOrder');
Route::post('/cart/order/complete','Site\OrderController@store')->name('storeStepOrder');
Route::get('cart','Site\CartController@index')->name('cart');
Route::post('cart','Site\CartController@remove')->name('cartRemove');
Route::post('/cart/update','Site\CartController@update')->name('cartUpdate');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/{url}', 'Site\PageController@index')->name('site.page.index');

Route::get('/public/{price}', 'Site\PriceController@download')->name('site.price.download');
