<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class SliderTest extends TestCase
{


    /**
     * A basic test example.
     *
     * @return void
     */
     public function testCreateSlider()
     {


        $pathToFile = "public/3288.JPG";

        $this->visit('/admin/slider/create')
                ->type('Новый Тестовый слайдер', 'name')
                ->attach($pathToFile, 'img')
                ->press('Добавить')
                ->seePageIs('/admin/slider');

        $seeInDatabase =  $this->seeInDatabase('sliders', ['img' => '3288.JPG']);
     }



    public function testUpdateSlider()
    {

       $editimg = "public/3288.JPG";

       $this->visit('/admin/slider/128/edit')
              ->type('Обновленное', 'name')
               ->attach($editimg, 'img')
               ->press('Изменить')
               ->seePageIs('/admin/slider/128/edit');

    }
}
