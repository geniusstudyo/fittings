<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class ProductTest extends TestCase
{


    /**
     * A basic test example.
     *
     * @return void
     */
     public function testCreateProduct()
     {


        $test_img = "public/3288.JPG";
        $test_prints = "public/32881.JPG";

        $this->visit('/admin/product/create')
                ->type('32322323', 'name')
                ->type('233223', 'url')
                ->type('2323', 'alias')
                ->type('3223', 'description')
                ->type('232323', 'price')
                ->attach($test_img, 'img')
                ->attach($test_prints, 'prints')
                ->type('323223', 'seo_title')
                ->type('3223', 'seo_desc')
                ->type('2323', 'seo_keywords')
                ->press('Добавить')
                ->seePageIs('/admin/product');

        $seeInDatabase =  $this->seeInDatabase('products', ['img' => '3288.JPG','prints'=>'32881.JPG']);
     }



    public function testUpdateProduct()
    {

      $test_img = "public/3288w.JPG";
      $test_prints = "public/3288a.JPG";

       $this->visit('/admin/product/9/edit')
               ->type('РомаТЕСТ', 'name')
               ->type('233223', 'url')
               ->type('2323', 'alias')
               ->type('3223', 'description')
               ->type('232323', 'price')
               ->attach($test_img, 'img')
               ->attach($test_prints, 'prints')
               ->type('323223', 'seo_title')
               ->type('3223', 'seo_desc')
               ->type('2323', 'seo_keywords')
               ->press('Изменить')
               ->seePageIs('/admin/product/9/edit');

    } 
}
