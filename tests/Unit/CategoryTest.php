<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class CategoryTest extends TestCase
{
    use WithoutMiddleware;
    /**
     * A basic test example.
     *
     * @return void
     */
     public function testCreateCategory()
     {


        $pathToFile = "public/3288.JPG";

        $this->visit('/admin/category/create')
                ->type('lkklk', 'name')
                ->type('lkklklk', 'desc')
                ->type('klklkl', 'url')
                ->type('ewwew', 'alias')
                ->attach($pathToFile, 'img')
                ->type('ewewew', 'title')
                ->type('weew', 'description')
                ->type('weewwe', 'keywords')
                ->press('Добавить')
                ->seeRouteIs('category.index');

        $seeInDatabase =  $this->seeInDatabase('categories', ['img' => '3288.JPG']);
     }



    public function testUpdateCategory()
    {

       $editimg = "public/3288.JPG";

       $this->visit('/admin/category/5/edit')
               ->type('lkklk', 'name')
               ->type('lkklklk', 'desc')
               ->type('klklkl', 'url')
               ->type('ewwew', 'alias')
               ->attach($editimg, 'img')
               ->type('ewewew', 'title')
               ->type('weew', 'description')
               ->type('weewwe', 'keywords')
               ->press('Изменить')
               ->seePageIs('/admin/category/5/edit');

    }
}
