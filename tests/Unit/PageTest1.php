<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class PageTest extends TestCase
{


    /**
     * A basic test example.
     *
     * @return void
     */
     public function testCreatePage()
     {


        // $pathToFile = "public/3288.JPG";

        $this->visit('/admin/page/create')
                ->type('Новая страница создана', 'name')
                ->type('url', 'url')
                ->type('seo_title','seo_title')
                ->type('seo_desc','seo_desc')
                ->type('seo_keywords','seo_keywords')
                // ->attach($pathToFile, 'img')
                ->press('Добавить')
                ->seePageIs('/admin/page');

        $seeInDatabase =  $this->seeInDatabase('pages', ['name' => 'Новая страница создана']);
     }



    public function testUpdatePage()
    {

       // $editimg = "public/3288.JPG";

       $this->visit('/admin/page/1/edit')
               ->type('Новая страница изменена', 'name')
               ->type('url1', 'url')
               ->type('seo_title1','seo_title')
               ->type('seo_desc1','seo_desc')
               ->type('seo_keywords1','seo_keywords')
               // ->attach($pathToFile, 'img')
               ->press('Изменить')
               ->seePageIs('/admin/page/1/edit');

      $seeInDatabase =  $this->seeInDatabase('pages', ['name' => 'Новая страница изменена']);
    }
}
