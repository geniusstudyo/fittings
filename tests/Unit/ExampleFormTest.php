<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleFormTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $this->assertTrue(true);
        $this->visit('/admin/menu/create')
              ->type('Название','name')
              ->type('URL','url')
              ->type('Alias','alias')
              ->type('title','title')
              ->type('description','description')
              ->type('keywords','keywords')
              ->press('Добавить')
              ->seePageIs('/admin/menu');

      $this->seeInDatabase('menus', ['name'=>'Название']);
    }
}
