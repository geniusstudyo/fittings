<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FileTest extends TestCase
{

    /**
     * A basic test example.
     *
     * @return void
     */
     public function testCreateFilePrice()
     {


        $pathToFile = "public/3288.JPG";

        $this->visit('/admin/price/create')
                ->attach($pathToFile, 'link')
                ->press('Добавить')
                ->seePageIs('/admin/price');

        $seeInDatabase =  $this->seeInDatabase('prices', ['link' => 'public\3288.JPG']);
     }



    
}
