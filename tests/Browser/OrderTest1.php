<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class OrderTest extends DuskTestCase
{

    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function test_visit_admin_order_and_see_order()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/admin/order')
                    ->assertSee('#ЗАКАЗ');
        });
    }

    public function test_visit_admin_order_and_press_create()
    {

        $this->browse(function (Browser $browser) {
            $browser->visit('/admin/order/create')
                   ->assertSee('Добавить заказ')
                  ->type('fio','Тестовое')
                  ->type( 'phone','380972388564')
                  ->type('city','Одесса')
                  ->type( 'street','Паустовского')
                  ->type('comment','Прошу доставить')
                  ->select('payments','paymentBank' )
                  ->select('status', 1)
                  ->type('comment', 'Комментарий к заказу')
                  ->press('Добавить')
                  ->assertPathIs('/admin/order');

        });

        $this->assertDatabaseHas('orders', [

                    'fio' => 'Тестовое'
                ]);

    }
}
