<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class MenuTest extends DuskTestCase
{

  /**
   * A Dusk test example.
   *
   * @return void
   */
  public function test_visit_admin_menu_and_see_menu()
  {
      $this->browse(function (Browser $browser) {
          $browser->visitRoute('menu.index')
                  ->assertSee('Главная');
      });
  }

  public function test_visit_admin_menu_and_press_create()
  {


      $this->browse(function (Browser $browser) {
          $browser->visitRoute('menu.create')
                 ->assertSee('Добавить пункт меню')
                ->type('name', 'Новый пункт меню')
                ->type('url', 'url')
                ->type('alias', 'alias')
                ->type('title', 'title')
                ->type('description', 'description')
                ->type('keywords', 'keywords')
                ->press('Добавить')
                ->assertRouteIs('menu.index');


      });

      $this->assertDatabaseHas('menus', [

                  'name' => 'Новый пункт меню'
              ]);

  }

}
